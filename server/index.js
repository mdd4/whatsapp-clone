const express = require("express");
const { Server } = require("socket.io");
const { createServer } = require("http");

const app = express();

const server = createServer(app);

const io = new Server(server);

// https://socket.io/docs/v4/emit-cheatsheet/
io.on("connection", (socket) => {
  console.log(`New connection ${socket.id}`);

  socket.broadcast.emit("newUser", socket.id);

  socket.on("disconnect", () => {
    console.log(`Socket ${socket.id} disconnected`);
  });

  socket.on("joinGroup", (groupId) => {
    socket.join(groupId);
  });

  socket.on("leaveGroup", (groupId) => {
    socket.leave(groupId);
  });
});

server.listen(8000, () => {
  console.log("Server is active on http://localhost:8000");
});
