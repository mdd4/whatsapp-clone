const socket = io("http://localhost:8000");

console.log(socket);

socket.on("connect", () => {
  console.log(socket.id); // "G5p5..."
});

socket.on("newUser", (user) => {
  console.log(`New user joined ${user}`);
});
