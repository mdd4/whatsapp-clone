# Install Server

In the server folder run:

1. `npm install express socket.io uuid --save`
2. `npm i -g nodemon`

## Run server

In the server folder run:

`nodemon .`

# Frontend

In the frontend folder run:

1. `npx serve .`
2. `npx livereload .`

## Get tailwind css

`npx tailwindcss -o tailwind.css`
